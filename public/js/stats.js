function drawFirstBallPie(firstBalls) {
  var firstStats = [['pins', 'percent']];
  for(something in firstBalls)
    firstStats.push([something, firstBalls[something]]);
  console.log(firstStats);
  console.log([
      ['Task', 'Hours per Day'],
      ['Work',     11],
      ['Eat',      2],
      ['Commute',  2],
      ['Watch TV', 2],
      ['Sleep',    7]
    ]);
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  // drawChart();
  function drawChart() {

    // var data = google.visualization.arrayToDataTable([
    //   ['Task', 'Hours per Day'],
    //   ['Work',     11],
    //   ['Eat',      2],
    //   ['Commute',  2],
    //   ['Watch TV', 2],
    //   ['Sleep',    7]
    // ]);

    var data = google.visualization.arrayToDataTable(firstStats);

    var options = {
      title: 'My Daily Activities'
    };

    var chart = new google.visualization.PieChart(document.getElementById('firstBallPie'));

    chart.draw(data, options);
  }
}