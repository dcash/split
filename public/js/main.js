function highlightFrame(frame, ball) {
  $('.ball1, .ball2, .ball3').removeClass('current-ball');
  $('#frame'+frame+' .ball'+ball).addClass('current-ball');
}

var gameSkip = 0;

var totalScore = 0;
var currentFrame = 1;
var currentBall = 1;
var ballOne = 0;
var frameScores = {1:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball2:0,
                      strike:false,
                      spare:false},
                   2:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball2:0,
                      strike:false,
                      spare:false},
                   3:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball2:0,
                      strike:false,
                      spare:false},
                   4:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball2:0,
                      strike:false,
                      spare:false},
                   5:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball2:0,
                      strike:false,
                      spare:false},
                   6:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball2:0,
                      strike:false,
                      spare:false},
                   7:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball2:0,
                      strike:false,
                      spare:false},
                   8:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball2:0,
                      strike:false,
                      spare:false},
                   9:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball2:0,
                      strike:false,
                      spare:false},
                   10:{score:0,
                      plus1:0,
                      plus2:0,
                      totalScore:0,
                      ball1:0,
                      ball2:0,
                      ball3:0,
                      strike:false,
                      spare:false}};

function disableButtons(pins) {
  for (var i = pins; i < 10; i++) {
    $('#score'+i).addClass('disabled');
  }
  if(arguments[1])
    $('#score10').addClass('disabled');
  else {
    $("#score10").attr('value', pins);
    $("#score10").text("spare");
  }
}

function enableButtons() {
  for (var i = 0; i <= 10; i++) {
    $('#score'+i).removeClass('disabled');
  }
  $("#score10").attr('value', 10);
  $("#score10").text("strike");
}

function addScore(score) {
  totalScore += score;
}

function removeScore() {
  if($('#saveGame').hasClass('disabled'))
    currentBall--;
  else {
    $('#saveGame').addClass('disabled');
    enableButtons();
  }
  if(currentBall == 0) {
    if(currentFrame == 1) {
      currentBall = 1;
      $("#scoreCLR").addClass('disabled');
    }
    else {
      currentBall = 2;
      currentFrame--;
    }
  }
  if(currentFrame < 10 && frameScores[currentFrame].strike) {
    frameScores[currentFrame].strike = false;
    currentBall = 1;
  }
  if(currentBall == 2) {
    if(currentFrame == 10) {
      if(frameScores[currentFrame]['ball1'] == 10){
        frameScores[10].score -= frameScores[currentFrame]['ball2'];
        ballOne = frameScores[currentFrame]['ball1'];
        enableButtons();
      }
      else {
        if(frameScores[currentFrame]['ball1'] + frameScores[currentFrame]['ball2'] < 10)
          totalScore -= frameScores[currentFrame]['ball2']+frameScores[currentFrame]['ball1'];
        disableButtons(10-frameScores[currentFrame]['ball1']);
      }
      if(frameScores[9].strike) {
        totalScore -= frameScores[9].score+frameScores[9].plus1+frameScores[9].plus2;
        frameScores[9].plus2 = 0;
        $('#frame'+(9)+' .total').html('');
      }
    }
    else {
      disableButtons(10-frameScores[currentFrame]['ball1']);
      if(frameScores[currentFrame].spare)
        frameScores[currentFrame].spare = false;
      else
        totalScore -= frameScores[currentFrame]['ball2']+frameScores[currentFrame]['ball1'];
      if(currentFrame > 1 && frameScores[currentFrame-1].strike) {
        totalScore -= frameScores[currentFrame-1].score+frameScores[currentFrame-1].plus1+frameScores[currentFrame-1].plus2;
        frameScores[currentFrame-1].plus2 = 0;
        $('#frame'+(currentFrame-1)+' .total').html('');
      }
    }
  }
  else if(currentBall == 3){
    if(frameScores[currentFrame]['ball2'] == 10 || frameScores[currentFrame]['ball1'] + frameScores[currentFrame]['ball2'] == 10)
      enableButtons();
    else
      disableButtons(10-frameScores[currentFrame]['ball2']);
    if(frameScores[currentFrame].ball1 + frameScores[currentFrame].ball2 == 10){
      totalScore -= frameScores[currentFrame].score+frameScores[currentFrame].plus2;
      frameScores[currentFrame].plus1 = 0;
    }
    else {
      totalScore -= frameScores[currentFrame].score+frameScores[currentFrame].plus1+frameScores[currentFrame].plus2;
      frameScores[currentFrame].plus2 = 0;
    }
  }
  else {
    enableButtons();
    if(currentFrame > 1 && frameScores[currentFrame-1].spare) {
      totalScore -= frameScores[currentFrame-1].score+frameScores[currentFrame-1].plus1;
      frameScores[currentFrame-1].plus1 = 0;
      $('#frame'+(currentFrame-1)+' .total').html('');
    }
    if(currentFrame > 1 && frameScores[currentFrame-1].strike) {
      frameScores[currentFrame-1].plus1 = 0;
      if(currentFrame > 2 && frameScores[currentFrame-2].strike) {
        totalScore -= frameScores[currentFrame-2].score+frameScores[currentFrame-2].plus1+frameScores[currentFrame-2].plus2;
        frameScores[currentFrame-2].plus2 = 0;
        $('#frame'+(currentFrame-2)+' .total').html('');
      }
    }
  }

  highlightFrame(currentFrame, currentBall);
  $('#frame'+currentFrame+' .ball'+currentBall).html("");
  $('#frame'+currentFrame+' .total').html("");
  frameScores[currentFrame]['ball'+currentBall] = 0;
}

function displayPinfall(frame, ball, pins) {
  if(frame == 10) {
    if(ball == 1) {
      if(pins == 10)
        $('#frame'+frame+' .ball'+ball).html("X");
      else
        $('#frame'+frame+' .ball'+ball).html(pins);
    }
    else if(ball >= 2) {
      if(ballOne < 10 && ballOne+pins == 10)
        $('#frame'+frame+' .ball'+ball).html("/");
      else if(pins == 10)
        $('#frame'+frame+' .ball'+ball).html("X");
      else
        $('#frame'+frame+' .ball'+ball).html(pins);
    }
  }
  else {
    if(ball == 1 && pins == 10)
      $('#frame'+frame+' .ball1').html("X");
    else if(ball == 2 && ballOne+pins == 10)
      $('#frame'+frame+' .ball2').html("/");
    else
      $('#frame'+frame+' .ball'+ball).html(pins);
  }
  frameScores[frame]['ball'+ball] = pins;
}

function displayScore(frame, score) {
  frameScores[frame].totalScore = score;
  $('#frame'+frame+' .total').html(score);
}

function checkStrike(pins) {
  if(pins == 10) {
    if(currentBall == 1) {
      frameScores[currentFrame].strike = true;
      frameScores[currentFrame].score = 10;
    }
    if(currentFrame < 10)
      currentFrame++;
    else {
      ballOne = pins;
      currentBall = 2;
    }
  }
  else {
    ballOne = pins;
    disableButtons(10-pins);
    currentBall = 2;
  }
}

function countStrikeSpare(frame, pins) {
  if(frameScores[frame].spare)
    frameScores[frame].plus1 = pins;
  else
    frameScores[frame].plus2 = pins;

  addScore(frameScores[frame].score+frameScores[frame].plus1+frameScores[frame].plus2);
  displayScore(frame, totalScore);
}

function findStrike() {

}

function recordScore(pins) {
  lastPinCount = pins;
  displayPinfall(currentFrame, currentBall, pins);

  if(currentFrame == 10) {
    if(currentBall == 1) {
        if(frameScores[9].strike) {
          frameScores[9].plus1 = pins;
          if(frameScores[8].strike)
            countStrikeSpare(8, pins);
          checkStrike(pins);
        }
        else if(frameScores[9].spare) {
          countStrikeSpare(9, pins);
          checkStrike(pins);
        }
        else
          checkStrike(pins);

        highlightFrame(10, 2);
    }
    else if(currentBall == 2) {
      if(frameScores[9].strike) {
        countStrikeSpare(9, pins);
        displayScore(9, totalScore);
      }
      if(ballOne + pins >= 10) {
        frameScores[10].score = ballOne + pins;
        currentBall = 3;
        highlightFrame(10, 3);
        if(pins < 10)
        {
          if(ballOne + pins == 10)
            enableButtons();
          else {
            disableButtons(10-pins);
            ballOne = pins;
          }
        }
        else {
          enableButtons();
          ballOne = pins;
        }
      }
      else {
        addScore(ballOne+pins);
        displayScore(10, totalScore);
        disableButtons(0, true);
        $('#saveGame').removeClass('disabled');
      }
    }
    else if(currentBall == 3) {
      countStrikeSpare(10, pins)
      disableButtons(0, true);
      $('#saveGame').removeClass('disabled');
    }
  }
  else {
    if(currentBall == 1) {
      if(currentFrame > 1) {
        if(frameScores[currentFrame-1].strike) {
          frameScores[currentFrame-1].plus1 = pins;
          if(currentFrame > 2) {
            if(frameScores[currentFrame-2].strike)
              countStrikeSpare(currentFrame-2, pins);
          }
          checkStrike(pins);
        }
        else if(frameScores[currentFrame-1].spare) {
          countStrikeSpare(currentFrame-1, pins);
          checkStrike(pins);
        }
        else
          checkStrike(pins);
      }
      else {
        checkStrike(pins);
        $("#scoreCLR").removeClass('disabled');
      }
    }
    else if(currentBall == 2) {
      if(currentFrame > 1) {
        if(frameScores[currentFrame-1].strike)
          countStrikeSpare(currentFrame-1, pins);
      }
      if(ballOne + pins == 10) {
        frameScores[currentFrame].spare = true;
        frameScores[currentFrame].score = 10;
      }
      else {
        addScore(ballOne+pins);
        displayScore(currentFrame, totalScore);
      }
      enableButtons();
      currentBall = 1;
      currentFrame++;
    }
    highlightFrame(currentFrame, currentBall);
  }
}


function saveGame() {
  var tmz = new Date().getTimezoneOffset();
  var opts = {lines: 11 // The number of lines to draw
            , length: 0 // The length of each line
            , width: 14 // The line thickness
            , radius: 30 // The radius of the inner circle
            , scale: 1 // Scales overall size of the spinner
            , corners: 1 // Corner roundness (0..1)
            , color: '#363636' // #rgb or #rrggbb or array of colors
            , opacity: 0 // Opacity of the lines
            , rotate: 0 // The rotation offset
            , direction: 1 // 1: clockwise, -1: counterclockwise
            , speed: 1 // Rounds per second
            , trail: 90 // Afterglow percentage
            , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
            , zIndex: 2e9 // The z-index (defaults to 2000000000)
            , className: 'spinner' // The CSS class to assign to the spinner
            , top: '40px' // Top position relative to parent
            , left: '50%' // Left position relative to parent
            , shadow: false // Whether to render a shadow
            , hwaccel: false // Whether to use hardware acceleration
            , position: 'absolute' // Element positioning
            }
  $('#saveWait').removeClass('hidden');
  var target = document.getElementById('saveWait');
  var spinner = new Spinner(opts).spin(target);
  var scoreboard = {1:{1:$('#frame1 .ball1').html(),
                       2:$('#frame1 .ball2').html(),
                       score:$('#frame1 .total').html()},
                    2:{1:$('#frame2 .ball1').html(),
                       2:$('#frame2 .ball2').html(),
                       score:$('#frame2 .total').html()},
                    3:{1:$('#frame3 .ball1').html(),
                       2:$('#frame3 .ball2').html(),
                       score:$('#frame3 .total').html()},
                    4:{1:$('#frame4 .ball1').html(),
                       2:$('#frame4 .ball2').html(),
                       score:$('#frame4 .total').html()},
                    5:{1:$('#frame5 .ball1').html(),
                       2:$('#frame5 .ball2').html(),
                       score:$('#frame5 .total').html()},
                    6:{1:$('#frame6 .ball1').html(),
                       2:$('#frame6 .ball2').html(),
                       score:$('#frame6 .total').html()},
                    7:{1:$('#frame7 .ball1').html(),
                       2:$('#frame7 .ball2').html(),
                       score:$('#frame7 .total').html()},
                    8:{1:$('#frame8 .ball1').html(),
                       2:$('#frame8 .ball2').html(),
                       score:$('#frame8 .total').html()},
                    9:{1:$('#frame9 .ball1').html(),
                       2:$('#frame9 .ball2').html(),
                       score:$('#frame9 .total').html()},
                    10:{1:$('#frame10 .ball1').html(),
                       2:$('#frame10 .ball2').html(),
                       3:$('#frame10 .ball3').html(),
                       score:$('#frame10 .total').html()},
                    total:totalScore,
                    timezone:tmz};

  $.ajax({ url: window.location.href.replace(window.location.pathname, '')+'/save-game',
      type: 'POST',
      data: scoreboard
    })
    .done(function(result) {
      $('#saveWait').addClass('hidden');
      spinner.stop();
      if(result.result == "success") {
        $('#saveFail').addClass('hidden');
        $('#saveSuccess').removeClass('hidden');
        $('#newGame').removeClass('hidden');
        $('#saveGame').addClass('hidden');
      }
      else {
        $('#saveFail').removeClass('hidden');
        $('#saveGame').text('Try Again');
      }
    })
    .fail(function() {
      $('#saveWait').addClass('hidden');
      spinner.stop();
      $('#saveFail').removeClass('hidden');
      $('#saveGame').text('Try Again');
    });
}

function show_games(skip) {
  if(skip == -1)
    gameSkip += 5;
  else if(skip == 1)
    gameSkip -= 5;

  if(gameSkip == 0) {
    $('#newer-games-btn').addClass('disabled');
  }
  else {
    $('#newer-games-btn').removeClass('disabled');
  }

  var opts = {lines: 13 // The number of lines to draw
            , length: 0 // The length of each line
            , width: 20 // The line thickness
            , radius: 42 // The radius of the inner circle
            , scale: 1 // Scales overall size of the spinner
            , corners: 1 // Corner roundness (0..1)
            , color: '#363636' // #rgb or #rrggbb or array of colors
            , opacity: 0 // Opacity of the lines
            , rotate: 0 // The rotation offset
            , direction: 1 // 1: clockwise, -1: counterclockwise
            , speed: 1 // Rounds per second
            , trail: 90 // Afterglow percentage
            , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
            , zIndex: 2e9 // The z-index (defaults to 2000000000)
            , className: 'spinner' // The CSS class to assign to the spinner
            , top: '45%' // Top position relative to parent
            , left: '50%' // Left position relative to parent
            , shadow: false // Whether to render a shadow
            , hwaccel: false // Whether to use hardware acceleration
            , position: 'absolute' // Element positioning
            }

  var table = $('#recent-games');
  table.css('min-height', '200px');
  table.empty();
  var target = document.getElementById('recent-games');
  var spinner = new Spinner(opts).spin(target);
  $.ajax({ url: 'https://splt.parseapp.com/retrieve-games/'+gameSkip,
    type: 'GET',
    dataType: 'json'
  })
  .done(function(data) {
    if(!data.moreGames) {
      $('#older-games-btn').addClass('disabled');
    }
    else {
      $('#older-games-btn').removeClass('disabled');
    }
    var games = data.games;

    if(Object.keys(games).length > 0) {
      $('#older-games-btn').removeClass('hidden');
      $('#newer-games-btn').removeClass('hidden');
      for(var date in games) {
        var series = games[date].scoreboards;
        var seriesTotal = games[date].total;
        var numGames = series.length;
        var seriesAvg = (seriesTotal/numGames).toFixed();

        var dateLink = '<a href="/stats/' + date.split('.').join('_') + 'T' + games[date].tmz + '">' + date + '</a>'

        table.append('<div class="row"><div class="col-md-12">'+dateLink+' | Avg: '+games[date].avg+'</div></div>');

        for(var j=0, k=numGames; j<k; j++){
          var game = series[j];
          var row = '<div class="row text-center">';
          var i = 1;
          while(i<10) {
            row += '<div id="frame'+i+'" class="col-xs-1 col-xs-ten frame">';

            row += '<div class="row">';
            row += '<div class="col-xs-12 head">'+i+'</div>';
            row += '</div>';
            
            row += '<div class="row">';
            if(game[i][1] == 'X' || game[i][2] == "/")
              row += '<div class="col-xs-6 ball1">'+game[i][1]+'</div>';
            else
              row += '<div class="col-xs-6 ball1 open-frame">'+game[i][1]+'</div>';
            row += '<div class="col-xs-6 ball2">'+game[i][2]+'</div>';
            row += '</div>';
            
            row += '<div class="row">';
            row += '<div class="col-xs-12 total">'+game[i].score+'</div>';
            row += '</div>';

            row += '</div>';

            i++;
          }

          row += '<div id="frame10" class="col-xs-1 col-xs-eleven frame">';

          row += '<div class="row">';
          row += '<div class="col-xs-12 head">10</div>';
          row += '</div>';
          
          row += '<div class="row">';
          if(game[10][1] == 'X' || game[10][2] == "/")
            row += '<div class="col-xs-4 ball1">'+game[10][1]+'</div>';
          else
            row += '<div class="col-xs-4 ball1 open-frame">'+game[10][1]+'</div>';
          if(game[10][2] == 'X' || game[10][3] == "/" || game[10][1] != 'X')
            row += '<div class="col-xs-4 ball2">'+game[10][2]+'</div>';
          else
            row += '<div class="col-xs-4 ball1 open-frame">'+game[10][2]+'</div>';
          row += '<div class="col-xs-4 ball3">'+game[10][3]+'</div>';
          row += '</div>';
          
          row += '<div class="row">';
          row += '<div class="col-xs-12 total">'+game[10].score+'</div>';
          row += '</div>';

          row += '</div>';

          row += '</div>';
          table.append(row);
        }
      }
    }
    else{
      table.append('<div class="col-md-12 text-center"><p class="large">You haven\'t recorded any games yet.</p></div>');
    }
    spinner.stop();
    table.css('min-height', '0px');
    table.css('display', 'none');
    table.fadeIn('slow');
  })
  .fail(function() {
    console.log("Something went wrong!");
    spinner.stop();
    table.css('min-height', '0px');
  });
}