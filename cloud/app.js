// These two lines are required to initialize Express in Cloud Code.
var express = require('express');
var parseExpressHttpsRedirect = require("parse-express-https-redirect");
var parseExpressCookieSession = require("parse-express-cookie-session");

var app = express();

var controllerDir = "cloud/controllers/";
var account = require(controllerDir+"account");
var game = require(controllerDir+"game");

module.exports = app;

// Global app configuration section
app.set('views', 'cloud/views');  // Specify the folder to find templates
app.set('view engine', 'jade');    // Set the template engine

app.use(parseExpressHttpsRedirect());
app.use(express.bodyParser());    // Middleware for reading request body
app.use(express.methodOverride("_method"));
app.use(express.cookieParser("splitcookiekey"));
app.use(parseExpressCookieSession({
  fetchUser: true,
  key: "split.sess",
  cookie: {
    maxAge: 3600000 * 24 * 30
  }
}));

app.get('/', account.index);
app.post('/', account.login);
app.get('/create-account', account.create);
app.post('/create-account', account.save);
app.get("/logout", account.logout);
app.get("/stats", account.stats);
app.get("/stats/:day", account.stats);
app.get("/add-game", requireLogin(), game.index);
app.post("/save-game", requireLogin(), game.save);

app.get('/retrieve-games/:skip', account.retrieve_games);


function requireLogin()  {
  return function(req, res, next) {
    if (Parse.User.current()) {
      next();
    } else {
      res.render('/', {error: 'You Must Be Logged In To Access This Page'});
    }
  }
};

// Attach the Express app to Cloud Code.
app.listen();