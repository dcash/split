require('cloud/app.js');

var Frame = Parse.Object.extend("Frame");
var Game = Parse.Object.extend("Game");

Parse.Cloud.define("getStats", function(request, response){
  var day = request.params.day || false;
  var user = Parse.User.current();
  var totalScore = 0;
  var firstPins = {0:0,
                   1:0,
                   2:0,
                   3:0,
                   4:0,
                   5:0,
                   6:0,
                   7:0,
                   8:0,
                   9:0,
                   'X':0};
  var frameStats = {1:{'strikes':0,'spares':0,'opens':0,'score':0,'balls':0,'avg':0},
                    2:{'strikes':0,'spares':0,'opens':0,'score':0,'balls':0,'avg':0},
                    3:{'strikes':0,'spares':0,'opens':0,'score':0,'balls':0,'avg':0},
                    4:{'strikes':0,'spares':0,'opens':0,'score':0,'balls':0,'avg':0},
                    5:{'strikes':0,'spares':0,'opens':0,'score':0,'balls':0,'avg':0},
                    6:{'strikes':0,'spares':0,'opens':0,'score':0,'balls':0,'avg':0},
                    7:{'strikes':0,'spares':0,'opens':0,'score':0,'balls':0,'avg':0},
                    8:{'strikes':0,'spares':0,'opens':0,'score':0,'balls':0,'avg':0},
                    9:{'strikes':0,'spares':0,'opens':0,'score':0,'balls':0,'avg':0},
                    10:{'strikes':0,'spares':0,'opens':0,'score':0,'balls':0,'avg':0}};
  var counts = {games:0,
               balls:0,
               firstBalls:0,
               secondBalls:0,
               secBall:0,
               avgOpen:0,
               strikes:0,
               spares:0,
               mostStrikes:0,
               streak:0,
               highScore:0};
  var gameQuery = new Parse.Query(Game);
  gameQuery.equalTo('user', Parse.User.current());
  gameQuery.include('frame_1');
  gameQuery.include('frame_2');
  gameQuery.include('frame_3');
  gameQuery.include('frame_4');
  gameQuery.include('frame_5');
  gameQuery.include('frame_6');
  gameQuery.include('frame_7');
  gameQuery.include('frame_8');
  gameQuery.include('frame_9');
  gameQuery.include('frame_10');
  if(day) {
    var date = day.split('T')[0].split('_').join('/');
    var tmz = day.split('T')[1];
    var startDate = new Date(new Date(date).getTime() + 60000*60*7);
    var startTime = new Date(startDate.getTime() + tmz*60*1000);
    var today = new Date(startTime);
    var tomorrow = new Date(startTime.setDate(startTime.getDate()+1));
    gameQuery.greaterThanOrEqualTo('createdAt', today);
    gameQuery.lessThan('createdAt', tomorrow);
  }
  gameQuery.each(function(game){
    var gameStreak = 0;
    var highStreak = 0;
    var gameStrikes = 0;

    for(i=1; i<10; i++) {
      var ballOne = game.get('frame_'+i).get('ball_one');
      counts.firstBalls++;
      counts.balls++;
      frameStats[i].balls++;
      if(ballOne == 'X') {
        firstPins['X']++;
        counts.strikes++;
        frameStats[i].strikes++;
        gameStrikes++;
        gameStreak++;
      }
      else {
        if(gameStreak > highStreak)
          highStreak = gameStreak;
        gameStreak = 0;
        counts.balls++;
        frameStats[i].balls++;
        counts.secondBalls++;
        firstPins[ballOne]++;
        var ballTwo = game.get('frame_'+i).get('ball_two');
        if(ballTwo == "/") {
          counts.spares++;
          frameStats[i].spares++;
        }
        else
          frameStats[i].opens++;
      }
      frameStats[i].score += Number(game.get('frame_'+i).get('score'));
    }

    var ballOne = game.get('frame_10').get('ball_one');
    counts.firstBalls++;
    counts.balls++;
    frameStats[10].balls++;
    if(ballOne == 'X') {
      firstPins['X']++;
      counts.strikes++;
      frameStats[10].strikes++;
      gameStrikes++;
      gameStreak++;
      counts.firstBalls++;
      counts.balls++;
      frameStats[10].balls++;
      var ballTwo = game.get('frame_10').get('ball_two');
      if(ballTwo == "X") {
        firstPins['X']++;
        counts.strikes++;
        frameStats[10].strikes++;
        gameStrikes++;
        gameStreak++;
        counts.balls++;
        frameStats[10].balls++;
        counts.firstBalls++;
        var ballThree = game.get('frame_10').get('ball_three');
        if(ballThree == 'X') {
          firstPins['X']++;
          counts.strikes++;
          frameStats[10].strikes++;
          gameStrikes++;
          gameStreak++;
        }
        else {
          firstPins[ballThree]++;
        }
      }
      else {
        if(gameStreak > highStreak)
          highStreak = gameStreak;
        gameStreak = 0;
        firstPins[ballTwo]++;
        counts.balls++;
        frameStats[10].balls++;
        counts.secondBalls++;
        var ballThree = game.get('frame_10').get('ball_three');
        if(ballThree == '/') {
          counts.spares++;
          frameStats[10].spares++;
        }
        else
          frameStats[10].opens++;
      }
    }
    else {
      if(gameStreak > highStreak)
        highStreak = gameStreak;
      gameStreak = 0;
      counts.balls++;
      frameStats[10].balls++;
      counts.secondBalls++;
      firstPins[ballOne]++;
      var ballTwo = game.get('frame_10').get('ball_two');
      if(ballTwo == "/") {
        counts.spares++;
        frameStats[10].spares++;
        counts.balls++;
        frameStats[10].balls++;
        counts.firstBalls++;
        var ballThree = game.get('frame_10').get('ball_three');
        if(ballThree == 'X') {
          firstPins['X']++;
          counts.strikes++;
          frameStats[10].strikes++;
          gameStrikes++;
          gameStreak++;
        }
        else {
          firstPins[ballThree]++;
          frameStats[10].opens++;
        }
      }
      else
        frameStats[10].opens++;
    }

    frameStats[i].score += Number(game.get('frame_10').get('score'));

    if(gameStrikes > counts.mostStrikes)
      counts.mostStrikes = gameStrikes;

    if(gameStreak > highStreak)
      highStreak = gameStreak;

    if(highStreak > counts.streak)
      counts.streak = highStreak;

    var gameScore = Number(game.get("score"));

    if(gameScore > counts.highScore)
      counts.highScore = gameScore;

    counts.games++;
    totalScore += gameScore;
  }).then(function(){
    var avgStrike, avgSpare = 0;
    var avg = (totalScore/counts.games).toFixed();
    if(counts.strikes > 0)
      avgStrike = ((counts.strikes/counts.firstBalls)*100).toFixed();
    if(counts.spares > 0)
      avgSpare = ((counts.spares/counts.secondBalls)*100).toFixed();

    var seconds = counts.firstBalls - counts.strikes;
    var opens = counts.secondBalls - counts.spares;

    var avgFirst = {};
    for(var pins in firstPins) {
      var freq = firstPins[pins];
      avgFirst[pins] = ((freq/counts.firstBalls)*100).toFixed();
    }

    counts.average = avg;
    counts.avgFirst = avgFirst;
    counts.avgSpare = avgSpare;
    counts.avgStrike = avgStrike;

    if(opens > 0)
      counts.avgOpen = ((opens/counts.secondBalls)*100).toFixed();
    if(seconds > 0)
      counts.secBall = ((seconds/counts.firstBalls)*100).toFixed();

    counts.strikesGame = (counts.strikes/counts.games).toFixed();
    counts.sparesGame = (counts.spares/counts.games).toFixed();
    counts.opensGame = (opens/counts.games).toFixed();

    for(var stat in frameStats) {
      frameStats[stat].avg = (frameStats[stat].score/counts.games).toFixed();
    }

    counts.frameStats = frameStats;

    response.success({stats:counts});
  });
});