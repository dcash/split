var viewDir = 'account';
var Game = Parse.Object.extend("Game");

exports.index = function(req, res) {
  if (Parse.User.current())
    res.render(viewDir);
  else
    res.render(viewDir+'/login', {title: 'Login', errorMessage:false});
};

exports.login = function(req, res) {
  Parse.User.logIn(req.body.username, req.body.password).then(function(user) {
    res.render(viewDir);
  }, function(error) {
    // Show the error message and let the user try again
    res.render(viewDir+'/login', {title: "Login Error", errorMessage: "Email or password incorrect." });
  });
};

exports.create = function(req, res) {
  res.render(viewDir+'/create', {errorMessage:false});
}

exports.save = function(req, res) {
  var user = new Parse.User();
  user.set("username", req.body.email);
  user.set("password", req.body.password);
  user.set("email", req.body.email);
  user.set("firstName", req.body.firstName);
  user.set("lastName", req.body.lastName);
  user.signUp(null, {
    success: function(user) {
      Parse.User.logIn(req.body.email, req.body.password).then(function(user) {
        res.render(viewDir);
      }, function(error) {
        // Show the error message and let the user try again
        res.render(viewDir+'/login', {title: "Login Error", errorMessage: "Email or password incorrect." });
      });
    },
    error: function(error) {
      res.render(viewDir+'/create', {errorMessage:"A new user could not be created at this time with the given information."});
    }
  });
}

exports.stats = function(req, res) {
  var day = req.params.day || false;

  Parse.Cloud.run('getStats', {day:day}, {
    success: function(result) {
      if(day)
        day = day.split('T')[0].split('_').join('.');
      res.render(viewDir+'/stats', {stats:result.stats, day:day});
    },
    error: function(error) {
      res.render(viewDir+'/stats', {stats:error.message, day:day});
    }
  });
}

exports.retrieve_games = function(req, res) {
  var skip = Number(req.params.skip);
  var allGames = {};
  var gameQuery = new Parse.Query(Game);
  gameQuery.equalTo('user', Parse.User.current());
  gameQuery.descending('createdAt');
  gameQuery.include('frame_1');
  gameQuery.include('frame_2');
  gameQuery.include('frame_3');
  gameQuery.include('frame_4');
  gameQuery.include('frame_5');
  gameQuery.include('frame_6');
  gameQuery.include('frame_7');
  gameQuery.include('frame_8');
  gameQuery.include('frame_9');
  gameQuery.include('frame_10');
  gameQuery.skip(skip);
  gameQuery.limit(5);
  gameQuery.find().then(function(games){
    if(games.length < 1)
      res.json({games:false, moreGames:false});
    else
      makeBoard(0, isComplete);

    function moreGames() {
      var gameQuery2 = new Parse.Query(Game);
      gameQuery2.equalTo('user', Parse.User.current());
      gameQuery2.descending('createdAt');
      gameQuery2.skip(skip+5);
      gameQuery2.limit(5);
      gameQuery2.find().then(function(more){
        if(more.length < 1)
          res.json({games:allGames, moreGames:false});
        else
          res.json({games:allGames, moreGames:true});
      });
    }

    function isComplete(index) {
      if(index == games.length)
        moreGames();
      else
        makeBoard(index, isComplete);
    }

    function makeBoard(index, response) {
      var tmz = games[index].get('timezone');
      var created = new Date(games[index].createdAt.getTime() - tmz*60*1000);
      var dateInfo = String(created).split(' ');
      var hour = Number(dateInfo[4].split(':')[0]);
      var date = dateInfo[1]+'.'+dateInfo[2]+'.'+dateInfo[3];
      if(hour < 7) {
        var lessDay = new Date(date);
        dateInfo = String(new Date(lessDay - 1)).split(' ');
        date = dateInfo[1]+'.'+dateInfo[2]+'.'+dateInfo[3];
      }
      var startDate = new Date(new Date(date).getTime() + 60000*60*7);
      var startTime = new Date(startDate.getTime() + tmz*60*1000);

      var scoreboard = {1:{1:games[index].get('frame_1').get('ball_one'),
                           2:games[index].get('frame_1').get('ball_two'),
                           score:games[index].get('frame_1').get('score')},
                        2:{1:games[index].get('frame_2').get('ball_one'),
                           2:games[index].get('frame_2').get('ball_two'),
                           score:games[index].get('frame_2').get('score')},
                        3:{1:games[index].get('frame_3').get('ball_one'),
                           2:games[index].get('frame_3').get('ball_two'),
                           score:games[index].get('frame_3').get('score')},
                        4:{1:games[index].get('frame_4').get('ball_one'),
                           2:games[index].get('frame_4').get('ball_two'),
                           score:games[index].get('frame_4').get('score')},
                        5:{1:games[index].get('frame_5').get('ball_one'),
                           2:games[index].get('frame_5').get('ball_two'),
                           score:games[index].get('frame_5').get('score')},
                        6:{1:games[index].get('frame_6').get('ball_one'),
                           2:games[index].get('frame_6').get('ball_two'),
                           score:games[index].get('frame_6').get('score')},
                        7:{1:games[index].get('frame_7').get('ball_one'),
                           2:games[index].get('frame_7').get('ball_two'),
                           score:games[index].get('frame_7').get('score')},
                        8:{1:games[index].get('frame_8').get('ball_one'),
                           2:games[index].get('frame_8').get('ball_two'),
                           score:games[index].get('frame_8').get('score')},
                        9:{1:games[index].get('frame_9').get('ball_one'),
                           2:games[index].get('frame_9').get('ball_two'),
                           score:games[index].get('frame_9').get('score')},
                        10:{1:games[index].get('frame_10').get('ball_one'),
                           2:games[index].get('frame_10').get('ball_two'),
                           3:games[index].get('frame_10').get('ball_three'),
                           score:games[index].get('frame_10').get('score')},
                        total:games[index].get('score')};

      if(allGames[date]) {
        allGames[date].scoreboards.push(scoreboard);
        index++;
        response(index);
      }
      else {
        allGames[date] = {};
        allGames[date].scoreboards = [scoreboard];
        allGames[date].tmz = tmz;
        getDayAverage(startTime, function(avg){
          allGames[date].avg = avg;
          index++;
          response(index);
        });
      }
    }
  });
}

exports.logout = function(req, res) {
  Parse.User.logOut();
  res.redirect('/');
};

function getDayAverage(day, response) {
  var today = new Date(day);
  var tomorrow = new Date(day.setDate(day.getDate()+1));

  var gameQuery = new Parse.Query(Game);
  gameQuery.equalTo('user', Parse.User.current());
  gameQuery.greaterThanOrEqualTo('createdAt', today);
  gameQuery.lessThan('createdAt', tomorrow);
  gameQuery.find().then(function(games){
    var total = 0;

    for (var i = 0, j = games.length; i < j; i++) {
      total += Number(games[i].get('score'));
    }

    response((total/j).toFixed());
  });
}