var Frame = Parse.Object.extend("Frame");
var Game = Parse.Object.extend("Game");

var viewDir = 'game';

exports.index = function(req, res) {
  res.render(viewDir+'/add');
};

exports.save = function(req, res) {
  var scoreboard = req.body;
  var frameArray = [];

  for (var i = 1; i <= 10; i++) {
    var frame = new Frame();
    frame.set('frame_no', i);
    frame.set('ball_one', scoreboard[i][1]);
    frame.set('ball_two', scoreboard[i][2]);
    if(scoreboard[i].hasOwnProperty(3))
      frame.set('ball_three', scoreboard[i][3]);
    frame.set('score', scoreboard[i].score);
    frameArray.push(frame);
  }

  Parse.Object.saveAll(frameArray, {
    success: function(frames) {
      var game = new Game();
      game.set('user', Parse.User.current());
      game.set('score', scoreboard.total);
      game.set('timezone', scoreboard.timezone);
      for (var i = 0; i < frames.length; i++) {
        game.set('frame_'+(i+1), frames[i]);
      }
      game.save(null, {
        success: function(savedGame) {
          res.json({result:"success"});
        },
        error: function(error) {
          res.json({result:"error"});
        }
      });
    },
    error: function(error) { 
      res.json({result:"error"});
    }
  });
};
